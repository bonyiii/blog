+++
title = 'Emacs Alternate File Command'
date = 2024-05-08T08:04:03+02:00
tags = ['Emacs', 'Lisp']
draft = false
+++

I long wanted to implement an alternate file opening in emacs. Previously I worked with a VIM setup
in which it was present, it was bound to `:AV` command and it was very convinient.

What do I mean by alternate file open? Assume you work with a Ruby on Rails project and editing a
model, in this case the test file corresponds to the given model is the alternate file and vice versa.

So the lips function should do the following:
* Parse the file path, select `app` and `.rb`
* Replace `app` with `spec`
* Replace `.rb` with `_spec.rb`
* Load the new file path into a variable
* Split the window vertically (in emacs-lisp terms horizontally)
* Switch to this new buffer and load the file content into it.

At first I settled with this solution:
It works from the `app` direction.
```
(defun open-rails-spec-file ()
  ;; Open corresponding test file
  (interactive)
  (let ((spec-file (replace-regexp-in-string "\\(.+\\)/app/\\(.*\\)\\.rb$" "\\1/spec/\\2_spec.rb" (file-truename buffer-file-name))))
    (message spec-file)
    (split-window-horizontally)
    (switch-to-buffer (find-file-noselect spec-file))))
```

Then I decided I want to push this forward and make it work from the `spec` folder direction too.

This is the function I am currently using:
```
(defun rails-alternate-file ()
  ;; Open corresponding test file
  (interactive)
  (if (string-match-p "spec/[^/]+/.+_spec\\.rb" (file-truename buffer-file-name))
      (let ((app-file (replace-regexp-in-string "\\(.+\\)/spec/\\(.*\\)_spec\\.rb$" "\\1/app/\\2.rb" (file-truename buffer-file-name))))
	(message app-file)
	(split-window-horizontally)
	(switch-to-buffer (find-file-noselect app-file)))
    (let ((spec-file (replace-regexp-in-string "\\(.+\\)/app/\\(.*\\)\\.rb$" "\\1/spec/\\2_spec.rb" (file-truename buffer-file-name))))
      (message spec-file)
      (split-window-horizontally)
      (switch-to-buffer (find-file-noselect spec-file)))))
```

I bounded the function in robe mode to `C-c o`. (eg: `CTRL+C` and after when you relesae that two button hit `o`)
```
(with-eval-after-load 'robe
  (define-key robe-mode-map (kbd "C-c o") 'rails-alternate-file))
```

So the function can now invoked to way: `M-x rails-alternate-file` or `C-c o`

To me it seems `C-c` is a common package specific command prefix.
