+++
title = 'Emacs on OpenSUSE Server'
date = 2024-04-23T08:09:48+02:00
draft = false
tags = ['Emacs', 'OpenSUSE']
+++

If you want to install emacs without X11 support on OpenSUSE, say for a VM or a server, then this is the package which provide it.

`zypper in emacs-nox`
