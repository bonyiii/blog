+++
title = 'Kvm enlarge OpenSUSE guest with btfs partition'
date = 2024-06-18T21:45:11+02:00
tags = ['KVM', 'OpenSUSE']
draft = false
+++

Say you have a KVM Guest in which you run minikube and suddenly minikube starts to complain about disk space.

On the host:

`qemu-img resize disk.qcow2 + 20G`

then, attach the disk a block device, [source](https://linuxconfig.org/how-to-resize-a-qcow2-disk-image-on-linux)
The magic is NBD, which stands for Network Block Device.

```
modprobe nbd max_part=10
qemu-nbd --connect /dev/nbd0 disk.qcow2
parted /dev/nbd0 -l
parted /dev/nbd0
```

Now here comes the tricky thing. Since we have 3 partion and the middle one (2nd) is the btrfs.
I decided to remove swap which was the 3 partition to avoid overlapping partition error.

Enlarged the 2 partition, eg, in parted
```
resize 2
```
important to add the metrics eg **GB**, I enlarged the partition end from 42.4GB to 62.4GB
then I recreated swap eg, still in parted https://askubuntu.com/questions/966394/how-do-i-create-a-swap-partition-using-parted

```
mkpart
Partition type? primary/extended? primary
File system type? [ext2]? linux-swap
Start? 62.4GB
End? 100%
```

Then still on the host

```
umount /mnt
qemu-nbd --disconnect /dev/nbd0
```

Then start the VM edit the grub config and remove `resume=/x/y/z part which tries to
restore data from swap partition` for now.

If all goes well the vm should boot up. Then https://linuxiac.com/how-to-resize-extend-kvm-virtual-disk-size/

Now btrfs magic, we are in the running VM:
```
btrfs filesystem resize max /
```

Wow by now we done with the most important part enlarged the filesystem. Actually a running file system!

run `mkswap /dev/vda3`

Get the UUID with blkid not the PARTUUID

```
/dev/vda3: UUID="82d7b0ce-be95-4ec2-971d-22bd0cf15ff3" TYPE="swap" PARTUUID="7cb31f04-2c1f-4a4a-bcb9-ad5dd232e9c2"
```

update /mnt/boot/grub2/grub.cfg and /etc/fstab with it. 
In grub.cfg the resume=X/Y/Z part should be updated

https://linuxier.com/how-to-find-uuid-in-linux/
