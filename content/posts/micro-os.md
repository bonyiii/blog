+++
title = 'Micro Os'
date = 2024-03-13T20:00:39+01:00
draft = true
+++

# Opensuse has a "new" distro called MicroOS

It is, according its websiste, designed to be a small environment to host containers or any other workload
that benefits from Transactional updates.

It sounds so good so I decided to give it a chance.

I found this tutorial: [openSUSE MicroOS: Painless Containerized Workflows for Lazy Developers](https://www.youtube.com/watch?v=CyodmkDbOW4)
It gives a great overview.

I never used transactional operation systems so I was curious how it works.

Instead of ```zypper install``` packages should be installed with ```transactional-update pkg install```.

Each installation creates a new snapshot and the system needs to be rebooted after a package installed.
If for example multiple packages should be installed at once it is possible still to create a single snapshot,
by using the ```-c``` switch.

```
  transactional-update -c pkg install etcd # then repeat as many times as you want, finallay reboot
  transactional-update -c pkg install mc
  reboot
```

Google search: micro os how to persist changes in /etc

Whatever changes made to the /etc folder
