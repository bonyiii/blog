+++
title = 'Kvm Old Guest Incomplete Logical Volume Partial Mode'
date = 2024-04-29T21:39:08+02:00
draft = false
tags = ['KVM', 'LVM', 'OpenSUSE']
+++

# Fix Incomplete Logical Volume Partial Mode on boot of an old KVM Guest

When I looked up this issue in Google it took me to Red Hat's page `How to fix the problem "PARTIAL MODE. Incomplete logical volumes will be processed" ?`
but the content is only available if you are a paid subscriber.

So I turned to Talk AI which returned this as a response:

> The error message "PARTIAL MODE. Incomplete logical volumes will be processed" typically occurs when there are logical volumes that are not fully accessible or available for processing. To fix this issue, follow these steps:
> 1.  Check the status of the logical volumes using the "lvdisplay" command to see which volumes are incomplete or inaccessible.
> 2.  If there are logical volumes that are in a partial state, try to fix them by running the "lvchange -ay" command to activate the logical volumes.
> 3.  If the above step doesn't resolve the issue, you may need to perform a repair on the logical volume. This can be done by running the "lvconvert --repair" command on the affected logical volumes.
> 4.  If the logical volumes are still not fully accessible, you may need to check for any underlying storage issues or hardware failures that could be causing the problem. Ensure that all physical volumes are online and accessible.
> 5.  Once you have resolved any underlying issues and ensured that all logical volumes are fully accessible, try running the command or operation that initially triggered the error message to see if the issue has been resolved.
> If you continue to experience the "PARTIAL MODE" error message after following these steps, consider seeking assistance from a professional system administrator or technical support to further investigate and resolve the issue.

I found this very helpful page how to mount the VM file system in rescue mode: https://forums.opensuse.org/t/repair-a-broken-uefi-grub2-opensuse-boot-scenario/129018

Then I got to this page: https://access.redhat.com/solutions/368783

but no, no solution, yet.
