+++
title = 'Docker and Docket Compose Autocomplete Bash'
date = 2024-05-18T18:51:43+02:00
draft = false
tags = ['OpenSUSE', 'Docker']
+++

Docker can generate it's completion config. You can turn it on system wide for all users, or just for a single user.

1; As `root`

`docker completion bash > /etc/bash_completion.d/docker-compose.sh`

2; As `regular user`, as recommended in docker's github :

`docker completion bash ~/.docker-compose.sh`

and add it to your .bashrc

`. ~/.docker-completion.sh`

