+++
title = 'Experiment with patroni cluster on OpenSUSE'
date = 2024-03-19T19:18:54+01:00
draft = false
tags = ['KVM', 'Patroni', 'HA', 'OpenSUSE']
+++

# Experiment with patroni cluster on OpenSUSE

This tutorial helps you get started with patroni, it's not a production grade
setup. We are going to use QEMU/KVM based virtualization for this setup.

## Setup KVM network

On Virtual Machine Manager GUI, add a new network name it as: cybernet

Then on the command line (it can be repeated anytime when you need to change something in cybernet config). 
If Virt Manager is running then it should be disconnected after the changes and connect back.

Edit cybernet config
```virsh net-edit --network cybernet```

Sample cybernet network configuration: update the MAC, IP addresses and node names according your setup
```xml
<network>
  <name>cybernet</name>
  <uuid>d500cd2f-38b3-45f7-a9a4-23a7a21c4a8c</uuid>
  <forward mode='nat'/>
  <bridge name='virbr1' stp='on' delay='0'/>
  <mac address='52:54:00:8d:0f:3d'/>
  <domain name='cbnet' localOnly='yes'/>
  <ip address='192.168.100.1' netmask='255.255.255.0'>
    <dhcp>
      <range start='192.168.100.128' end='192.168.100.254'/>
      <host mac='52:54:00:19:86:2a' name='etcd-1' ip='192.168.100.169'/>
      <host mac='52:54:00:8c:78:4d' name='patroni-1' ip='192.168.100.200'/>
      <host mac='52:54:00:1a:c9:9b' name='patroni-2' ip='192.168.100.232'/>
      <host mac='52:54:00:ef:fb:02' name='patroni-3' ip='192.168.100.245'/>
      <host name='ha-virtual-ip' ip='192.168.100.33'/>
      <host mac='52:54:00:d3:12:fc' name='ha-1' ip='192.168.100.228'/>
    </dhcp>
  </ip>
</network>
```

Clear existing network settings, if any, for the cybernet network
```bash
virsh net-destroy --network cybernet
```

Start the network with the new settings
```bash
virsh net-start --network cybernet
```

## Setup VM for ETCD
First setup a VM with etcd. In this setup we are going to setup a single
instance etcd cluster, since our focus is on Patroni.

Etcd will be the brain which supervise our cluster, it is where the status of the cluster is stored.

### Install the packages
```
zypper in etcd etcdctl
```

Checkout official ETCD documentation it has an excellent [Demo](https://etcd.io/docs/v3.4/demo/ "ETCD 3.4 Demo page") section which can be used
as a basis.

### Open etcd ports on the VM Firewall
```
firewall-cmd --zone=public --add-port=2379/tcp --permanent
firewall-cmd --zone=public --add-port=2380/tcp --permanent

firewall-cmd --list-all # list setings before applying them
firewall-cmd --reload
firewall-cmd --list-all # list setings after applying them
```

```--zone``` means to which zone we would like to add the rule

```--permanent``` makes sure the rule will be saved and applied on reboot

```--add-port``` opens the necessary port

In order to active these setting ```firewall-cmd --reload``` needs to be run

If you would like to know more about firewall-cmd [Cyberciti](https://www.cyberciti.biz/faq/set-up-a-firewall-using-firewalld-on-opensuse-linux/) has a great article in the topic.

### Make sure etcd starts now and on boot
```systemctl enable --now etcd.service```


## Setup Patroni-1 VM

SSH into the VM as root or login as regular user and switch to root.

```bash
ssh 192.168.100.200 -l root
```

Install patroni, postgresql and necessary tools

```bash
zypper in postgresql-server-devel gcc python3-devel
pip install patroni[psycopg3,etcd3]
```

Create a home directory for our patroni postgresql

```bash
mkdir /home/patroni -p
chown postgres:postgres /home/patroni
```

Open the firewall port
```bash
firewall-cmd --zone=public --add-port=5432/tcp --permanent # Postgresql port
firewall-cmd --zone=public --add-port=8008/tcp --permanent # Patroni port (HA Proxy will check status on it)

firewall-cmd --reload
```

Cd into ```/home/patroni``` folder.

Patroni can generate a [sample config](https://patroni.readthedocs.io/en/latest/patroni_configuration.html#sample-patroni-configuration) for us with the following command:

```patroni --generate-sample-config [configfile]```

The patroni.yml below was created this way. Values were adjusted to match with our requirements:
```yaml
scope: home-lab
namespace: /db/
name: partoni-1

log:
  format: '%(asctime)s %(levelname)s: %(message)s'
  level: INFO
  max_queue_size: 1000
  traceback_level: ERROR

restapi:
  connect_address: 192.168.100.200:8008
  listen: 192.168.100.200:8008

etcd3:
  hosts:
    - 192.168.100.169:2379

# The bootstrap configuration. Works only when the cluster is not yet initialized.
# If the cluster is already initialized, all changes in the `bootstrap` section are ignored!
bootstrap:
  # This section will be written into <dcs>:/<namespace>/<scope>/config after initializing
  # new cluster and all other cluster members will use it as a `global configuration`.
  # WARNING! If you want to change any of the parameters that were set up
  # via `bootstrap.dcs` section, please use `patronictl edit-config`!
  dcs:
    loop_wait: 10
    retry_timeout: 10
    ttl: 30
    postgresql:
      parameters:
        hot_standby: 'on'
        max_connections: 100
        max_locks_per_transaction: 64
        max_prepared_transactions: 0
        max_replication_slots: 10
        max_wal_senders: 10
        max_worker_processes: 8
        track_commit_timestamp: 'off'
        wal_keep_size: 128MB
        wal_level: replica
        wal_log_hints: 'on'
      use_pg_rewind: true
      use_slots: true

postgresql:
  authentication:
    replication:
      password: password
      username: replicator
    rewind:
      password: password
      username: rewind_user
    superuser:
      password: password
      username: postgres
  bin_dir: ''
  connect_address: 192.168.100.200:5432
  data_dir: 'mydata'
  listen: 192.168.100.200:5432
  parameters:
    password_encryption: scram-sha-256
  pg_hba:
  - host all all all scram-sha-256
  - host replication replicator all scram-sha-256

tags:
  clonefrom: true
  failover_priority: 1
  noloadbalance: false
  nosync: false
```

When done, check there are no mistakes.
```patroni --validate-config```

Start the patroni manually to test if everything is fine:

```bash
cd /home/patroni
sudo -u postgres patroni patroni.yml
```

The output should be something like this:
```bash
WARNING:root:Failed to obtain address: gaierror(-2, 'Name or service not known')
INFO:patroni.dcs.etcd:Selected new etcd server http://192.168.100.169:2379
2024-04-04 14:20:17,680 INFO: Selected new etcd server http://192.168.100.169:2379
INFO:patroni.postgresql.config:No PostgreSQL configuration items changed, nothing to reload.
2024-04-04 14:20:17,697 INFO: No PostgreSQL configuration items changed, nothing to reload.
INFO:patroni.ha:Lock owner: None; I am partoni-1
INFO:patroni.__main__:trying to bootstrap a new cluster
2024-04-04 14:20:17,763 INFO: Lock owner: None; I am partoni-1
2024-04-04 14:20:17,780 INFO: trying to bootstrap a new cluster
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with this locale configuration:
  provider:    libc
  LC_COLLATE:  C
  LC_CTYPE:    en_US.UTF-8
  LC_MESSAGES: C
  LC_MONETARY: C
  LC_NUMERIC:  C
  LC_TIME:     C
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

Data page checksums are disabled.

creating directory mydata ... ok
creating subdirectories ... ok
selecting dynamic shared memory implementation ... posix
selecting default max_connections ... 100
selecting default shared_buffers ... 128MB
selecting default time zone ... Europe/Budapest
creating configuration files ... ok
running bootstrap script ... ok
performing post-bootstrap initialization ... ok
syncing data to disk ... INFO:patroni.ha:Lock owner: None; I am partoni-1
INFO:patroni.ha:not healthy enough for leader race
2024-04-04 14:20:27,704 INFO: Lock owner: None; I am partoni-1
2024-04-04 14:20:27,706 INFO: not healthy enough for leader race
INFO:patroni.__main__:bootstrap in progress
2024-04-04 14:20:27,724 INFO: bootstrap in progress
ok

initdb: warning: enabling "trust" authentication for local connections
initdb: hint: You can change this by editing pg_hba.conf or using the option -A, or --auth-local and --auth-host, the next time you run initdb.

Success. You can now start the database server using:

    pg_ctl -D mydata -l logfile start

INFO:patroni.postgresql.postmaster:postmaster pid=13697
2024-04-04 14:20:30,997 INFO: postmaster pid=13697
2024-04-04 14:20:31.057 CEST   [13697]LOG:  redirecting log output to logging collector process
2024-04-04 14:20:31.057 CEST   [13697]HINT:  Future log output will appear in directory "log".
192.168.100.200:5432 - no response
192.168.100.200:5432 - accepting connections
192.168.100.200:5432 - accepting connections
INFO:patroni.postgresql.connection:establishing a new patroni heartbeat connection to postgres
2024-04-04 14:20:32,091 INFO: establishing a new patroni heartbeat connection to postgres
INFO:patroni.__main__:running post_bootstrap
2024-04-04 14:20:32,155 INFO: running post_bootstrap
WARNING:patroni.watchdog.base:Could not activate Linux watchdog device: Can't open watchdog device: [Errno 13] Permission denied: '/dev/watchdog'
2024-04-04 14:20:32,761 WARNING: Could not activate Linux watchdog device: Can't open watchdog device: [Errno 13] Permission denied: '/dev/watchdog'
INFO:patroni.__main__:initialized a new cluster
2024-04-04 14:20:32,961 INFO: initialized a new cluster
INFO:patroni.ha:Lock owner: partoni-1; I am partoni-1
INFO:patroni.__main__:no action. I am (partoni-1), the leader with the lock
2024-04-04 14:20:33,144 INFO: no action. I am (partoni-1), the leader with the lock
```

After this with ```CTRL+C``` the instance can be stopped.

What happens is patroni detects mydata folder was not present and postgres doesn't run it invokes initdb and then start postgres server.


## Setup Patroni-2 VM

Repeat the steps defined in [Setup VM for Patroni-1](#setup-patroni-1-vm)

First make sure to start Patroni-1 again, then start Patroni-2. Patroni-2 is going to join to the cluster as follower.

```bash
patroni-2:/home/patroni # sudo -u postgres patroni patroni.yml
2024-04-06 18:08:44,903 INFO: Selected new etcd server http://192.168.100.169:2379
2024-04-06 18:08:44,908 INFO: No PostgreSQL configuration items changed, nothing to reload.
2024-04-06 18:08:45,112 INFO: Lock owner: partoni-1; I am partoni-2
2024-04-06 18:08:45,120 INFO: trying to bootstrap from leader 'partoni-1'
pg_basebackup: error: could not initiate base backup: ERROR:  the standby was promoted during online backup
HINT:  This means that the backup being taken is corrupt and should not be used. Try taking another online backup.
pg_basebackup: removing data directory "mydata"
2024-04-06 18:08:45,227 ERROR: Error when fetching backup: pg_basebackup exited with code=1
2024-04-06 18:08:45,230 WARNING: Trying again in 5 seconds
2024-04-06 18:08:45,616 INFO: Lock owner: partoni-1; I am partoni-2
2024-04-06 18:08:45,652 INFO: bootstrap from leader 'partoni-1' in progress
2024-04-06 18:08:55,619 INFO: Lock owner: partoni-1; I am partoni-2
2024-04-06 18:08:55,622 INFO: bootstrap from leader 'partoni-1' in progress
2024-04-06 18:09:02,081 INFO: replica has been created using basebackup
2024-04-06 18:09:02,082 INFO: bootstrapped from leader 'partoni-1'
2024-04-06 18:09:02,782 INFO: postmaster pid=8061
192.168.100.232:5432 - no response
2024-04-06 18:09:02.835 CEST   [8061]LOG:  redirecting log output to logging collector process
2024-04-06 18:09:02.835 CEST   [8061]HINT:  Future log output will appear in directory "log".
192.168.100.232:5432 - accepting connections
192.168.100.232:5432 - accepting connections
2024-04-06 18:09:03,816 INFO: Lock owner: partoni-1; I am partoni-2
2024-04-06 18:09:03,817 INFO: establishing a new patroni heartbeat connection to postgres
2024-04-06 18:09:03,881 INFO: no action. I am (partoni-2), a secondary, and following a leader (partoni-1)
2024-04-06 18:09:05,615 INFO: no action. I am (partoni-2), a secondary, and following a leader (partoni-1)
```

Let's see our cluster
```bash
sudo -u postgres patronictl -c /home/patroni/patroni.yml list
+ Cluster: home-lab (7353985676912522612) ----------+----+-----------+----------------------+
| Member    | Host            | Role    | State     | TL | Lag in MB | Tags                 |
+-----------+-----------------+---------+-----------+----+-----------+----------------------+
| partoni-1 | 192.168.100.200 | Leader  | running   |  4 |           | clonefrom: true      |
|           |                 |         |           |    |           | failover_priority: 1 |
+-----------+-----------------+---------+-----------+----+-----------+----------------------+
| partoni-2 | 192.168.100.232 | Replica | streaming |  4 |         0 | clonefrom: true      |
|           |                 |         |           |    |           | failover_priority: 1 |
+-----------+-----------------+---------+-----------+----+-----------+----------------------+
```

## Setup HA Proxy VM

Install haproxy
```bash
zypper in haproxy
```

HA Proxy config on OpenSUSE 15.5 can be found at ```/etc/haproxy/haprox.cfg```
```ini
global
  log /dev/log daemon
  maxconn 32768
  chroot /var/lib/haproxy
  user haproxy
  group haproxy
  daemon
  stats socket /var/lib/haproxy/stats user haproxy group haproxy mode 0640 level operator
  tune.bufsize 32768
  tune.ssl.default-dh-param 2048
  ssl-default-bind-ciphers ALL:!aNULL:!eNULL:!EXPORT:!DES:!3DES:!MD5:!PSK:!RC4:!ADH:!LOW@STRENGTH

defaults
  log     global
  mode    http
  option  log-health-checks
  option  log-separate-errors
  option  dontlog-normal
  option  dontlognull
  option  httplog
  option  socket-stats
  retries 3
  option  redispatch
  maxconn 10000
  timeout connect     5s
  timeout client     50s
  timeout server    450s

listen stats
  bind 0.0.0.0:8404
  #bind :::80 v6only
  stats enable
  stats uri     /
  stats refresh 5s

listen postres_primary
  bind *:5000
  option httpchk OPTIONS /primary
  http-check expect status 200
  default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions  
  server patroni-1 192.168.100.200 maxconn 100 check port 8008
  server patroni-2 192.168.100.232 maxconn 100 check port 8008
  server patroni-3 192.168.100.45 maxconn 100 check port 8008

listen postgres_replica
  bind *:5001
  option httpchk OPTIONS /replica
  http-check expect status 200
  default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions  
  server patroni-1 192.168.100.200 maxconn 100 check port 8008
  server patroni-2 192.168.100.232 maxconn 100 check port 8008
  server patroni-3 192.168.100.45 maxconn 100 check port 8008
```

In the config above, we check ```/primary``` and ```/replica``` endpoints on the patroni/postgres nodes so that we can distinguish
which node fullfils which role. Read more about the health check endpoints in the [official documentation](https://patroni.readthedocs.io/en/latest/rest_api.html#health-check-endpoints).

Validate the config file
```bash
haproxy -c -f /etc/haproxy/haproxy.cfg
```

You can read more about [validating HA Proxy config](https://www.haproxy.com/blog/testing-your-haproxy-configuration) on the official HAProxy blog.

Open firewall
```bash
firewall-cmd --add-port=8404/tcp --zone=public --permanent
firewall-cmd --reload
```
