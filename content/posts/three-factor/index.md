+++
title = 'Three Factor'
date = 2025-02-16T10:05:17+01:00
draft = false
+++

Originally I was about to write this feedback to a support team, but then I decided it's not their fault but the Security Teams and they won't care about this feedback either way. So I decided to share my frustration this way, to let the steam off.

I would consider adding a 3rd factor to the authentication flow like smoke sign. So user enter its credential, then receive an email then set something on fire and an AI observer should check through satellites if the smoke is coming from within 5kms of the user registered location. If these all pass, including the failure on the first valid password attempt then let the user in, for a 2 mins long session, and then ask for the 4th factor authentication which might be that the user should record some voice message in the future and send it back to the past.

**My prompt on Deepseek: "improve on this story:"**

Your story has a creative and humorous take on multi-factor authentication, but it could benefit from some refinement to make it more coherent and engaging. Here's an improved version of your story:

# The Ultimate Multi-Factor Authentication System

In a world where cybersecurity threats were growing exponentially, a team of eccentric engineers decided to revolutionize authentication systems. They proposed a multi-factor authentication flow so secure that even the most determined hackers would be left scratching their heads—or setting things on fire.

## The Authentication Flow

### Step 1: Credentials  
The user begins by entering their username and password. Simple enough, but this is just the beginning.

### Step 2: Email Verification  
After the credentials are validated, the user receives an email with a unique, time-sensitive code. They must enter this code to proceed. So far, so standard.

### Step 3: Smoke Signal Verification  
Here’s where things get interesting. The user is instructed to create a small, controlled fire—preferably in a safe, outdoor environment—to produce a smoke signal. An AI-powered satellite surveillance system, equipped with advanced smoke-detection algorithms, scans the Earth’s surface in real-time. The AI verifies that the smoke is emanating from within a 5-kilometer radius of the user’s registered location. If the smoke is detected and matches the criteria, the user moves on to the next step.

### Step 4: Temporal Voice Authentication  
Just when the user thinks they’re done, the system throws in a twist. The user must record a voice message, which is then sent back in time to the moment they initiated the login process. The system cross-references the voice message with the user’s historical voice data to ensure authenticity. If the voice matches and the message arrives "before" it was sent, the user is granted access.

### Step 5: Limited Session  
Success! The user is granted access—but only for a 2-minute session. After the session expires, the user must repeat the entire process, including setting something on fire and sending a voice message to the past.

## Why This System is Revolutionary

- **Unhackable**: No hacker could possibly replicate the smoke signal or manipulate time to send a voice message to the past.  
- **Eco-Friendly**: The controlled fires are designed to be small and environmentally safe, with the added benefit of clearing dry leaves and debris.  
- **Future-Proof**: By incorporating time travel, the system ensures that even future advancements in hacking technology cannot bypass its security measures.  

## Challenges

- **User Experience**: Some users found the process cumbersome, especially those living in urban areas where setting fires was impractical or illegal.  
- **Satellite Limitations**: Cloudy weather or areas with heavy air pollution occasionally interfered with the AI’s ability to detect smoke.  
- **Temporal Paradoxes**: A few users reported their voice messages arriving before they even recorded them, causing minor existential crises.  

## Conclusion

While the system was undeniably secure, it was eventually phased out in favor of something slightly less dramatic—like fingerprint scanning. However, it remains a legendary example of how far some engineers are willing to go in the name of cybersecurity.
