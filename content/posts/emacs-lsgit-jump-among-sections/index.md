+++
title = 'Emacs Lsgit Jump Among Sections'
date = 2025-01-11T14:40:56+01:00
tags = ['Emacs']
draft = false
+++

I was struggling to be able swith among sections in `helm-ls-git`

I have this keybinding set up: `(global-set-key (kbd "M-l") 'helm-ls-git)`

But I wasn't able to switch among git status, git files, buffers, git branches, etc.
Those are rendered as separate blocks and I wasn't able to switch from one to another,
it was pretty frustrating. Finally deepseek pointed me to the right direction with the
below snipet. So now I am able to manully switch among the sections. Next step would be to 
be able automatically move to the next section if I am at the end of the current one.

```elisp
(with-eval-after-load 'helm
  (define-key helm-map (kbd "M-p") 'helm-previous-source)
  (define-key helm-map (kbd "M-n") 'helm-next-source))
```
