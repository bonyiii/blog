+++
title = 'Emacs Failed to Verify Signature'
date = 2024-06-16T22:27:19+02:00
tags = ['Emacs']
draft = false
+++

When you run emacs `package-list-packages` command and run into this issue:

```Bash
1 Failed to verify signature archive-contents.sig:
2 No public key for 645357D2883A0966 created at 2024-06-16T11:05:03+0200 using EDDSA
3 Command output:
4 gpg: Signature made Sun 16 Jun 2024 11:05:03 AM CEST
5 gpg:                using EDDSA key 0327BE68D64D9A1A66859F15645357D2883A0966
6 gpg: Can't check signature: No public key
```

Then here is the solution: https://emacs.stackexchange.com/questions/233/how-to-proceed-on-package-el-signature-check-failure

## TL;DR

### In emacs, turn off the signature check :)
`M-: (setq package-check-signature nil) RET`

### Download new signatures:
`M-x package-install RET gnu-elpa-keyring-update RET`

The above threw me an error, so as a fallback I went with

`M-x package-list-packages`

in there I searched with `C-s` for `gnu-elpa-keyring-update` and installed it from there.

### Reset signature check:
`M-: (setq package-check-signature 'allow-unsigned)`

It throwed me some error but after restart everything worked just fine



## Still, from StackExchange:
As stated in the package, the following holds:

If your keys are already too old, causing signature verification errors when installing packages, then in order to install this package you can do the following:

    Fetch the new key manually, e.g. with something like:

    gpg --homedir ~/.emacs.d/elpa/gnupg      \
         --keyserver hkp://keys.openpgp.org  \
         --receive-keys 066DAFCB81E42C40

    Modify the expiration date of the old key, e.g. with something like:

    gpg --homedir ~/.emacs.d/elpa/gnupg \
        --quick-set-expire 474F05837FBDEF9B 1y

    temporarily disable signature verification (see variable `package-check-signature').
