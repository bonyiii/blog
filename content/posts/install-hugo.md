+++
title = 'Install Hugo'
date = 2024-03-08T11:41:48+01:00
draft = false
+++

# Install Hugo for the first time in 2024

After following the steps recommended on the official site I run into this error:

```Error: error building site: TOCSS: failed to transform "/ananke/css/main.css" (text/css). Check your Hugo installation; you need the extended version to build SCSS/SASS with transpiler set to 'libsass'.: this feature is not available in your current Hugo version, see https://goo.gl/YMrWcn for more information```

## TL;DR
For linux amd64 this is the extended version which worked for me:

[hugo_extended_0.123.8_linux-amd64.tar.gz](https://github.com/gohugoio/hugo/releases/download/v0.123.8/hugo_extended_0.123.8_Linux-64bit.tar.gz)

[All Releases](https://github.com/gohugoio/hugo/releases/tag/v0.123.8)

## Details

After running into the issue above it is striked me this is a perfect opportunity to write the first blog post about it.
Though, it was not only me who [realized](https://github.com/gohugoio/hugoDocs/issues/1549) this is indeed an issue.

I looked up the issue on google which took me to [Hugo discourse](https://discourse.gohugo.io/t/failed-to-transform-ananke-css-main-css-text-css/33990) page.
Unfortunately only thing I found there is a recommendation to read the manual.
I understand this approach, and the frustrations on both side. Someone put a lot of effort to push this great project forward and rightfully expect users, 
who would simply would like to take advantage of the author's work for free, read the documentation. The truth is this is partially the case. 
My personal way of doing is that I read the quickstart, maybe a the TL;DR part and try to move forward with my thing. For example I would like to share 
my knowledge with others and I would like to user a blogging system for that.


### Tthis is a pattern!
I realized, it reccurs all the time. For me this is the most important part, how can this situation improved?

Most of the time people just want to start using a product, regardless it is free or paid, but it is often overwhelmingly 
complicated to even get started. One of the possible reason I can identify so far is the frequent changes either in the project or in its dependencies.
