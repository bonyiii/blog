+++
title = 'Stop Policy Kit Automount'
date = 2024-03-19T19:18:54+01:00
draft = false
tags = ['KDE', 'OpenSUSE']
+++

# How to disable Policy kit automount on OpenSUSE 15.4

If you receive an annoying pop up after each login, which says "Authentication is required to mount ..."

Something like:

![Authentication is required to mount](images/policyKit_automount_prompt.png)

Then open System Settings -> click on Removable Storages

![System Settigns](images/system_settings.png)

Remove the tick from checkboxes in "On Login" and "On Attach" columns and apply settings.

![Removable Devices](images/policyKit_removable_devices.png)

This is how my current setup looks like after the change:

![Removable Devices After](images/policyKit_removable_devices_after.png)

Credit goes [keybreak & endeavouros.com](https://forum.endeavouros.com/t/how-to-stop-password-prompt-for-partition-mounting-from-happening/11493)
