+++
title = 'Find and Replace String in Rails Project'
date = 2024-09-07T19:46:30+02:00
tags = ['Rails', 'Bash']
draft = false
+++

Say you need to rename a Ruby Class name recursively in a rails project.


```Bash
find . -type f -iname *.rb -exec sed -i 's/Somehting/Namespaced::Something/g' {} ;
```

For further details check out this [StackOverflow](https://stackoverflow.com/questions/1583219/how-can-i-do-a-recursive-find-replace-of-a-string-with-awk-or-sed) thread.
