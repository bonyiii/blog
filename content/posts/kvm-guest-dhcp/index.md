+++
title = 'Kvm Guest Dhcp'
date = 2024-04-28T22:40:34+02:00
tags = ['KVM']
draft = false
+++

# Change IP address of a linux machine manually.

My KVM DHCP server assign random IP to the guest VMs which is ok by default but onward
in the past thi IP didn't change too often so it sticked with the MAC address even without
forcing it with `virsh net-edit`.

So I hadd to figure out how to change IP address of a linux machine with the ip command.

Say it has `192.168.122.211` assigned but I want `192.168.122.24` becuase that is what I have in the host machine `/etc/hosts` file as an alias for the VM and it is easier to type the name than the IP.

First I assign the new IP:

`ip address add 192.168.122.24/24 dev eth0` or the same using shorthands `ip a a 192.168.122.24/24 dev eth`

check the addresss
`ip address` or `ip a`

then remove the unnecessary address
`ip address del 192.168.122.211/24 dev eth0`

