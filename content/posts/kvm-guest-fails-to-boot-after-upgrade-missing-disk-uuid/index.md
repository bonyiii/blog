+++
title = 'Kvm Guest Fails to Boot After Upgrade Missing Disk Uuid'
date = 2024-12-28T21:32:00+01:00
tags = ['KVM', 'OpenSUSE']
draft = false
+++

I run into this strange issue. I have a KVM Guest which happend to be Opensuse 15.5 Leap with BTRFS. Actually I think none of these matters too much.

This is what the guest `blkid` comamnd returns, before the upgrade:

```shell
/dev/vda2: UUID="8eb75df0-d2de-46ba-a0ee-da03af917144" UUID_SUB="d591fab2-2141-4e12-b99a-f594c4a4ab63" BLOCK_SIZE="4096" TYPE="btrfs" PARTUUID="b6749583-bf3a-4f31-b364-e3d786e8040c"
/dev/vda3: UUID="82d7b0ce-be95-4ec2-971d-22bd0cf15ff3" TYPE="swap" PARTUUID="7cb31f04-2c1f-4a4a-bcb9-ad5dd232e9c2"
/dev/vda1: PARTUUID="cf1ac418-650d-42f5-81c6-3bcab31b7cc1"
```

After the upgrade though this is what I got:


```shell
/dev/vda2: UUID="8eb75df0-d2de-46ba-a0ee-da03af917144" UUID_SUB="d591fab2-2141-4e12-b99a-f594c4a4ab63" BLOCK_SIZE="4096" TYPE="btrfs" PARTUUID="b6749583-bf3a-4f31-b364-e3d786e8040c"
/dev/vda3: UUID="82d7b0ce-be95-4ec2-971d-22bd0cf15ff3" TYPE="swap" PARTUUID="7cb31f04-2c1f-4a4a-bcb9-ad5dd232e9c2"
/dev/vda1: PARTUUID="cf1ac418-650d-42f5-81c6-3bcab31b7cc1"
```

Which is the same...


The problem is that I have this line in `/etc/default/grub`

`GRUB_CMDLINE_LINUX_DEFAULT="splash=silent resume=/dev/disk/by-uuid/c94dc377-efd1-4569-a394-95f506a88edc preempt=full mitigations=auto quiet security=apparmor"`

Now, it instructs grub to resume data stored in this swap partion, but that doesn't exists. The strange thing is it seems it didn't exists even previously. Maybe it's only created while the system is beign shut down and removed when it's booted up?

This is how [DeepSpeek](https://chat.deepseek.com/sign_in) explains what `resume` is in `grub`:

> In GRUB, the resume parameter is used to specify the swap device or swap file that the system should use for hibernation (suspend-to-disk). When a system hibernates, the current state of the system (including RAM contents) is saved to the swap device or file. During the next boot, the system "resumes" from this saved state instead of performing a fresh boot.

So, for now, I just removed the resume part

```
# From
GRUB_CMDLINE_LINUX_DEFAULT="splash=silent resume=/dev/disk/by-uuid/c94dc377-efd1-4569-a394-95f506a88edc preempt=full mitigations=auto quiet security=apparmor"

# To
GRUB_CMDLINE_LINUX_DEFAULT="splash=silent preempt=full mitigations=auto quiet security=apparmor"
```

and re-generate the grub config by running:

`grub2-mkconfig -o /boot/grub2/grub.cfg`
